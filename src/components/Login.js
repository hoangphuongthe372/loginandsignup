import React, {Component} from 'react';


class Login extends Component{
    constructor(){
        super();
        this.state = {
            email: '',
            password : ''
        }
        
        this.onHandleChange = this.onHandleChange.bind(this);
        this.onHandleSubmit = this.onHandleSubmit.bind(this);
    }
    
    onHandleChange(event){
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    onHandleSubmit(event){
        console.log("Email: " + this.state.email + " - Password:" + this.state.password);
        event.preventDefault();
    }


    render(){
        return(
            <form onSubmit = { this.onHandleSubmit }>
                <h3>Login</h3>

                <div className="form-group">
                    <label>Email</label>
                    <input type="email" className="form-control" name = "email" value={this.state.email} placeholder="Nhập Email" onChange={this.onHandleChange} />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" name = "password" value={this.state.password} placeholder="Nhập password" onChange={this.onHandleChange}/>
                </div>

                <div className="form-group">
                    <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="customCheck1" />
                        <label className="custom-control-label" htmlFor="customCheck1">Nhớ mật khẩu</label>
                    </div>
                </div>

                <button type="submit" className="btn btn-primary btn-block">Đăng nhập</button>
                <p className="forgot-password text-right">
                    Quên <a href="/#">mật khẩu?</a>
                </p>
            </form>
        );
    }
}

export default Login;