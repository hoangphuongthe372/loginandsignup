import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';

class Signup extends Component{
    constructor(){
        super();
        this.state = {
            isRedirect : false,
            name: '',
            email: '',
            password : '',
            confirmPassword : ''
        }

        this.onHandleChange = this.onHandleChange.bind(this);
        this.onHandleSubmit = this.onHandleSubmit.bind(this);
    }
    onHandleChange(event){

        this.setState({
            [event.target.name]: event.target.value
        })

    }

    onHandleSubmit(event){
        if(this.state.password === this.state.confirmPassword){
            console.log(" FullName: " + this.state.name + " - Email: " + this.state.email + " - Password:" + this.state.password);
            event.preventDefault();
        }else{
            alert("Mật khẩu không trùng khớp !");
        }

        this.setState({
            isRedirect : true
        });

    }

    render(){

        if(this.state.isRedirect){
            return(
                <Redirect to="/sign-in" />
            );
        }
        return(
            <form onSubmit = { this.onHandleSubmit }>
                <h3>Sign Up</h3>


                <div className="form-group">
                    <label>Họ và tên</label>
                    <input type="text" className="form-control" name="name" placeholder="Nguyễn Văn A" onChange={this.onHandleChange}/>
                </div>

                <div className="form-group">
                    <label>Email</label>
                    <input type="email" className="form-control" name="email" placeholder="Nhập email" onChange={this.onHandleChange}/>
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" name="password" placeholder="Nhập password" onChange={this.onHandleChange} />
                </div>

                <div className="form-group">
                    <label>Xác nhận password</label>
                    <input type="password" className="form-control" name="confirmPassword" placeholder="Nhập lại password" onChange={this.onHandleChange} />
                </div>

                <button type="submit" className="btn btn-primary btn-block">Đăng ký</button>
                <p className="forgot-password text-right">
                    Đã <a href="/#">đăng ký?</a>
                </p>
            </form>
        );
    }
}

export default Signup;